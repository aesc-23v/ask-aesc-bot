from typing import Optional
from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
import json

from config import TOKEN, ADMIN_CHAT_ID, QUOTE_CHANNEL_ID


bot = Bot(token=TOKEN)
dp = Dispatcher(bot)


def get_json():
    with open("wait.json", "r") as inp:
        file = json.loads(inp.read())
    # print(file)
    return file


def update_json(upd):
    with open("wait.json", "w") as out:
        out.write(json.dumps(upd, indent=4))


HELP_STRING: str = """
Сообщение отправляет предложение на рассмотрение
"""

ADMIN_STRING: str = """
/approve - в ответ на сообщение с предложением, выкладывает в канал
/reject - в ответ на сообщение с предложением, отклоняет
/reject_all - отклоняет все предложения
"""


@dp.message_handler(commands=["help"])
async def process_help(msg: types.Message):
    if msg.chat.id == ADMIN_CHAT_ID:
        await msg.reply(ADMIN_STRING)
    else:
        await msg.reply(HELP_STRING)


@dp.message_handler(commands=["approve"])
async def approve_msg(msg: types.Message):
    if msg.chat.id != ADMIN_CHAT_ID:
        return

    offer = msg.reply_to_message
    if offer is None:
        return

    id = str(offer.message_id)
    now = get_json()
    if id not in now:
        await msg.reply("This message is not an offer!")
        return

    await bot.send_message(QUOTE_CHANNEL_ID, now[id][0])
    await msg.reply(f"Successfully approved offer from {now[id][1]}")
    del now[id]
    update_json(now)


@dp.message_handler(commands=["reject"])
async def reject_msg(msg: types.Message):
    if msg.chat.id != ADMIN_CHAT_ID:
        return

    offer = msg.reply_to_message
    if offer is None:
        return

    id = str(offer.message_id)
    now = get_json()
    if id not in now:
        await msg.reply("This message is not an offer!")
        return

    await msg.reply(f"Successfully rejected offer from {now[id][1]}")
    del now[id]
    update_json(now)


@dp.message_handler(commands=["reject_all"])
async def reject_all_msg(msg: types.Message):
    update_json({})
    await msg.reply(f"You have just rejected all offers :(")


@dp.message_handler()
async def fwd_message(msg: types.Message):
    if msg.chat.id == ADMIN_CHAT_ID:
        return

    offer = msg.text

    sender = msg.from_user.username
    if sender == None:
        sender = msg.from_user.full_name
    else:
        sender = "@" + sender

    sent = await bot.send_message(ADMIN_CHAT_ID, f'Offer from {sender}:\n{offer}')
    now = get_json()
    now[sent.message_id] = (offer, sender)
    update_json(now)

if __name__ == '__main__':
    with open("wait.json", "w") as out:
        out.write("{\n}")
    executor.start_polling(dp)
