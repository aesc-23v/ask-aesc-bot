from os import getenv

TOKEN = getenv('BOT_TOKEN')
ADMIN_CHAT_ID = getenv('BOT_CHAT_ID')
QUOTE_CHANNEL_ID = getenv('BOT_CHANNEL_ID')
